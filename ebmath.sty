% This package contains a handful of tools for math typesetting that I often
% need. It does not modify the aspect of the document in any way.

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{ebmath}[2015/04/29 EB's math tools]

% * Macros using variable-size delimiters
%
% When defining macros for notations, it is frequent to use delimiters.
% Prototypical examples include notations for sets in comprehension, or the
% bra and ket notations of quantum mechanics. It is natural to define such
% macros using the |\left| and |\right| primitives, but it is sometimes
% desirable to use explicit size modifiers like |\big| and |\Big| to get
% better results. The following macros unify the two approaches in order to
% make macros that can use one form or the other.
%
% \varleft, \varmiddle, \varright :
%   These are used like \left, \middle, \right but they may behave like static
%   versions like \bigl, \bigm, \bigr according to the previous use of
%   \setdelimsize.
% \varnoleft, \varnoright :
%   Should be used when no delimiter is used, instead of \left. and \right.
% \setdelimsize{SIZE} :
%   Set up the macros above, for the current group, according to SIZE:
%   0 - variable size, adapts to the contents between \varleft and \varright
%   1 - normal size
%   2,3,4,5 - fixed sizes like big, Big, bigg, Bigg
%
% \delimiters{LEFT}{RIGHT}[SIZE]{CONTENTS}
%   Provides a short-hand for simple delimiters. The CONTENTS are put between
%   LEFT and RIGHT using the given SIZE. If SIZE is omitted, variable size is
%   used. This form can be used for macro definitions like this:
%
%     \newcommand\bra{\delimiters{\langle}{|}}
%
%   Then \bra will have an optional argument for the size and a mandatory
%   argument for the contents.
%
% \delimitersplit{LEFT}{MIDDLE}{RIGHT}[SIZE]{CONTENTS1}{CONTENTS2}
%   Provides a short-hand for delimiters with a separator: this renders like
%   LEFT CONTENTS1 MIDDLE CONTENTS2 RIGHT using the specified SIZE. For
%   instance, sets in comprehension can be defined as
%
%     \newcommand\set{\delimitersplit{\{}{|}{\}}}
%
%
% * Syntax definitions
%
% The "syntax" environment provides a high-level presentation for BNF-like
% syntax definitions. It is used in text mode and produces a displayed table.
% The usage is as follows:
%
% \begin{syntax}
%   \define[optional text] symbols
%     \case first case  \comment{optional text}
%     \case second case
%     \altcase third case
%     ...
%   \define other symbols
%     \case first case
%     ...
% \end{syntax}
%
% The optional texts are set in text mode, the symbols and definitions are set
% in inline math mode. The macro |\define| introduces the definition of a
% syntactic class, it is followed by the symbols defined. Each case of the
% definition is introduced either by |\case| or by |\altcase|. With |\case|,
% the case begins a new line (except right after |\define| where it stays on
% the same line). With |\altcase|, the case is put on the same line as the
% previous one (except when |\comment| was used).
%
%
% * Miscellaneous
%
% \restrict[SIZE]{FUNCTION}{DOMAIN}
%   The vertical bar notation for function restrictions, with an optional size
%   parameter with the same meaning as above.
%
% \begin{formulas}[<setup>] ... \end{formulas}
%   A series of displayed formulas, centered and spaced, with automatic line
%   breaking between formulas. Use |\split| for a separation between formulas
%   with optional line break, and |\break| for an imperative line break. Both
%   commands take an optional argument that will be typeset as text in the
%   middle of the separation or after the line break. The optional argument
%   <setup> contains commands that are evaluated before typesetting the first
%   formula, in a group that ends after the final line break; typical usage
%   includes setting dimensions like |\lineskip|, for proper line spacing.
%
%
% * Inserting math symbols in the text
%
% \BoldMathIfBold :
%   Act like \boldmath if the current font is bold, do nothing otherwise.
% \Math{CODE} :
%   Typeset the CODE in math mode, in bold if the context is bold.
% \DeclareTextMath{SEQUENCE}[ALTERNATE]{CODE}
%   Declare the control SEQUENCE as a math-mode symbol to be used as text. The
%   CODE will be typeset in bold math if the surrounding text is bold. The
%   optional argument specifies a pure text equivalent for contexts where math
%   is not acceptable, i.e. in a PDF index. The command is defined as robust,
%   so it can be used safely in section titles, etc.
%
%-----------------------------------------------------------------------------

%%%  Variable size delimiters

\newcommand\varleft{\left}
\ifx\middle\relax
  \newcommand\varmiddle{\mathord}
\else
  \newcommand\varmiddle{\middle}
\fi
\newcommand\varright{\right}

\newcommand\setdelimsize[1]{%
  \def\varnoleft{\varleft.}%
  \def\varnoright{\varright.}%
  \ifcase#1%
    \let\varleft=\left \let\varmiddle=\middle \let\varright=\right
  \or
    \def\varleft{\mathopen\eb@delimiter}%
    \def\varmiddle##1{\mathclose{}\eb@delimiter{##1}\mathopen{}}
    \def\varright{\mathclose\eb@delimiter}%
    \let\varnoleft\relax
    \let\varnoright\relax
  \or \let\varleft=\bigl  \let\varmiddle=\big  \let\varright=\bigr
  \or \let\varleft=\Bigl  \let\varmiddle=\Big  \let\varright=\Bigr
  \or \let\varleft=\biggl \let\varmiddle=\bigg \let\varright=\biggr
  \or \let\varleft=\Biggl \let\varmiddle=\Bigg \let\varright=\Biggr
  \fi}

\def\eb@delimiter#1{{\hbox{$\left#1{}\right.\n@space$}}}

% Short-hands

\newcommand\delimiters[2]{%
  \@ifnextchar[{\eb@delimiters{#1}{#2}}{\eb@delimiters{#1}{#2}[0]}}
\def\eb@delimiters#1#2[#3]#4{%
  {\setdelimsize{#3}\varleft#1#4\varright#2}}

\newcommand\varmiddlerel[1]{%
  \mathrel{}\varmiddle#1\mathrel{}}

\newcommand\delimitersplit[3]{%
  \@ifnextchar[{\eb@delimitersplit{#1}{#2}{#3}}{\eb@delimitersplit{#1}{#2}{#3}[0]}}
\def\eb@delimitersplit#1#2#3[#4]#5#6{%
  {\setdelimsize{#4}\varleft#1#5\varmiddlerel{#2}#6\varright#3}}

%%% Miscellaneous

\newcommand\restrict[3][0]{%
  {\setdelimsize{#1}\varnoleft{#2}\varmiddle|_{#3}\varnoright}}

\def\formulas@split{%
  \@ifnextchar[\formulas@split@text\formulas@split@notext}
\def\formulas@split@text[#1]{%
  $\hskip .5em plus .5fil{#1}\penalty10000\hskip .5em plus .5fil$\displaystyle}
\def\formulas@split@notext{%
  $\hskip 1em plus 1fil$\displaystyle}

\def\formulas@break{%
  \@ifnextchar[\formulas@break@text\formulas@break@notext}
\def\formulas@break@text[#1]{%
  $\penalty-10000{#1}\hfil$\displaystyle}
\def\formulas@break@notext{%
  $\penalty-10000$\displaystyle}

\newenvironment{formulas}[1][]{%
  \center
  \let\split\formulas@split
  \let\break\formulas@break
  \lineskip=2ex\relax
  #1$\displaystyle}{$%
  \endcenter}


%%% Syntax definitions

% For |\text|:
\RequirePackage{amsmath}
% For configuration:
\RequirePackage{pgfkeys}

% Parameters

\pgfqkeys{/ebmath/syntax}{
% low-level elements
open/.initial={\begin{displaymath}},
close/.initial={\end{displaymath}\@endpetrue\@ignoretrue},
equals/.initial=,
or/.initial=,
alt/.initial=,
% predefined styles
light/.style={
  equals={{}:={}},
  or=,
  alt={,\mskip8mu}},
bnf/.style={
  equals={{}::={}},
  or={|\mskip12mu{}},
  alt={\mskip12mu|\mskip12mu}},
inline/.style={
  open=$,
  close=$},
% default style
light
}

\def\syntax@sym@equals{\pgfkeysvalueof{/ebmath/syntax/equals}}
\def\syntax@sym@or{\pgfkeysvalueof{/ebmath/syntax/or}}
\def\syntax@sym@alt{\pgfkeysvalueof{/ebmath/syntax/alt}}

% Detecting where we are

\def\syntax@colnum{0}
% 0 = left text
% 1 = defined symbol
% 2 = definition
% 3 = right text

\def\syntax@switch#1#2#3#4{%
  \begingroup
  \ifcase\syntax@colnum
  \def\temp{#1}\or
  \def\temp{#2}\or
  \def\temp{#3}\or
  \def\temp{#4}\fi
  \expandafter\endgroup\temp}

% Commands in the environment

\newcommand\syntax@define[1][]{%
  \syntax@switch{}{\\}{\\}{\\}%
  \text{#1}&%
  \def\syntax@colnum{1}}

\def\syntax@case{%
  \syntax@switch{&\syntax@sym@or&}{\syntax@sym@equals&}%
    {\\&\syntax@sym@or&}{\\&\syntax@sym@or&}%
  \def\syntax@colnum{2}}

\def\syntax@altcase{%
  \syntax@switch{&\syntax@sym@or&}{\syntax@sym@equals&}%
    {\syntax@sym@alt}{\\&\syntax@sym@or&}%
  \def\syntax@colnum{2}}

\def\syntax@comment#1{%
  \syntax@switch{&&&}{&&}{&}{\\&&&}%
  \text{#1}%
  \def\syntax@colnum{3}\relax}

\newenvironment{syntax}[1][]{%
  \let\define\syntax@define
  \let\case\syntax@case
  \let\altcase\syntax@altcase
  \let\comment\syntax@comment
  \pgfqkeys{/ebmath/syntax}{#1}%
  \pgfkeysvalueof{/ebmath/syntax/open}%
  \begin{array}{l@{\quad}r@{}l@{\quad}l}%
}{%
  \end{array}%
  \pgfkeysvalueof{/ebmath/syntax/close}%
}


%%% Math symbols in the text

\newcommand\BoldMathIfBold{%
% from the definition of \LaTeXe:
    \if b\expandafter\@car\f@series\@nil\boldmath\fi}

\newcommand\Math[1]{%
  {\BoldMathIfBold\ensuremath{#1}}}
\newcommand\DeclareTextMath[1]{%
  \@ifnextchar[{\DeclareTextMath@alt{#1}}{\DeclareTextMath@noalt{#1}}}
\def\DeclareTextMath@noalt#1#2{%
  \DeclareRobustCommand{#1}{{\BoldMathIfBold$#2$}}}
\def\DeclareTextMath@alt#1[#2]#3{%
  \DeclareRobustCommand{#1}{\eb@texorpdfstring{{\BoldMathIfBold$#3$}}{#2}}}

\let\eb@texorpdfstring=\@firstoftwo
\AtBeginDocument{%
  \@ifpackageloaded{hyperref}{\relax%
    \let\eb@texorpdfstring=\texorpdfstring}{}}
