% vim: syntax=tex

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ebarticlef}[2010/09/10 EB's article format, in French]

\LoadClassWithOptions{ebarticle}

\RequirePackage[frenchb]{babel}

\renewcommand\definitionname{D\'efinition}
\renewcommand\notationname{Notation}
\renewcommand\examplename{Exemple}
\renewcommand\remarkname{Remarque}
\renewcommand\theoremname{Th\'eor\`eme}
\renewcommand\propositionname{Proposition}
\renewcommand\lemmaname{Lemme}
\renewcommand\corollaryname{Corollaire}
