% vim: syntax=tex

% Document class for exercise sheets (labs, homeworks, etc.)
%
% author: Emmanuel Beffara <manu@beffara.org>
%
% General format:
%
%   \documentclass[options]{ebsheet}
%   \header{header for the first page}  % optional
%   \course{course title}               % optional
%   \title{sheet title}
%   \date{as usual}                     % optional
%   \begin{document}
%   \maketitle
%     ...
%   \end{document}
%
% Format of exercises: see exo.sty.
% By default, hints are typeset upside down and answers are ignored.


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ebsheet}[2012/10/15 EB's format for exercise sheets]

% General format

\newcommand\ebsheet@fontsize{11pt}
\DeclareOption{10pt}{\def\ebsheet@fontsize{10pt}}
\ProcessOptions\relax

\LoadClass[a4paper,\ebsheet@fontsize]{article}

\RequirePackage{amsthm}

\ifdefined\XeTeXrevision
  \RequirePackage{xltxtra}
\else
  \RequirePackage[T1]{fontenc}
\fi

\RequirePackage[a4paper,width=15cm,height=22cm]{geometry}

% \RequirePackage{charter,euler}
% \RequirePackage[scaled]{beramono}
% \RequirePackage{fourier}

% Title format

\RequirePackage{ifthen}

\newcommand\header[1]{\gdef\@header{#1}}
\def\@header{}

\newcommand\course[1]{\gdef\@course{#1}}
\def\@course{}

\def\@date{}

\def\@maketitle{%
  \newpage
  \null
  \ifthenelse{\equal{\@header}{}}{}{%
    \begin{flushleft}\small
      \@header
    \end{flushleft}}%
  \vskip 2em%
  \begin{center}%
  \let \footnote \thanks
    \ifthenelse{\equal{\@course}{}}{}{%
      {\Large \@course \par}%
      \vskip 1em}%
    {\LARGE \@title \par}%
    \vskip 1em%
    {\large \@date \par}%
  \end{center}%
  \par
  \vskip 1.5em}

% Sections

\RequirePackage[medium]{titlesec}

% Pages are not numbered by default
% (say \pagestyle{numbered} for the standard style).

\let\ps@numbered=\ps@plain
\let\ps@plain=\ps@empty
\ps@empty

% Numbering and format for exercises

\RequirePackage{graphicx,xcolor}
\RequirePackage[spacing=sheet,inline=false,hints=reversed,answers=ignored]{exo}
\ExoConfig{hintsetup=\small\color{gray}}
\def\exo@space@before{%
  \par\penalty-100\vskip 3ex plus 1fil minus 1ex\relax}

% Numbered definitions and theorems

\theoremstyle{definition}
\newtheorem{definition}{\definitionname}
\newtheorem{notation}[definition]{\notationname}
\theoremstyle{remark}
\newtheorem{example}{\examplename}
\theoremstyle{plain}
\newtheorem{theorem}{\theoremname}
\newtheorem{proposition}[theorem]{\propositionname}
\newtheorem{lemma}[theorem]{\lemmaname}
\newtheorem{corollary}[theorem]{\corollaryname}

\providecommand\definitionname{Definition}
\providecommand\notationname{Notation}
\providecommand\examplename{Example}
\providecommand\remarkname{Remark}
\providecommand\theoremname{Theorem}
\providecommand\propositionname{Proposition}
\providecommand\lemmaname{Lemma}
\providecommand\corollaryname{Corollary}
